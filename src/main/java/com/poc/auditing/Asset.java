package com.poc.auditing;

/**
 * Created by rjilani on 10/8/2015.
 */
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Asset {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String assetName;
    private String assetLocation;

    protected Asset() {}

    public Asset(String assetName, String assetLocation) {
        this.assetName = assetName;
        this.assetLocation = assetLocation;
    }

    @Override
    public String toString() {
        return String.format(
                "Asset[id=%d, assetName='%s', assetLocation='%s']",
                id, assetName, assetLocation);
    }

    public String getAssetName() {

        return assetName;
    }

    public String getAssetLocation() {

        return assetLocation;
    }

    public void setAssetName(String assetName) {

        this.assetName = assetName;
    }

    public void setAssetLocation(String assetLocation) {

        this.assetLocation = assetLocation;
    }

    public long getId() {
        return id;
    }
}