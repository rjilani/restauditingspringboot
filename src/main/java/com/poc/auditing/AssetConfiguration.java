package com.poc.auditing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AssetConfiguration {

	private static final Logger log = LoggerFactory.getLogger(AssetConfiguration.class);

	@Bean
	public CommandLineRunner demo(AssetRepository repository) {
		return (args) -> {

			log.info("-------------test-----------------");

			// save a couple of Assets
			repository.save(new Asset("HouseOfCard", "http://www.scrippsnetwork.com/houseofcards"));
			repository.save(new Asset("ShawsahnkRedemption", "http://www.scrippsnetwork.com/shawshankredemption"));
			repository.save(new Asset("UsualSuspect", "http://www.scrippsnetwork.com/usuualsuspect"));
			repository.save(new Asset("KillAMockingBird", "http://www.scrippsnetwork.com/killamockingbird"));
			repository.save(new Asset("LegendOfTheFalls", "http://www.scrippsnetwork.com/legendsofthefall"));
			log.info("Audit logging for Asset:");

			log.info("Adding records to repository");

			log.info("--------------test---------------");
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(AssetConfiguration.class, args);
	}

}
