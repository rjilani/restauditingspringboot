package com.poc.auditing;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/assetrepository")
public class AssetController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    private static final Logger log = LoggerFactory.getLogger(AssetController.class);

    @Autowired
    private AssetRepository repository;

    @Autowired
    private AuditLog auditLog;

    @RequestMapping(method=RequestMethod.GET, value = "/getAssets/{id}")
    public ResponseEntity<Asset> getAssetById(@PathVariable long id) {

        // fetch an individual Asset by ID
        Asset asset = repository.findOne(id);
        System.out.println("Asset found with findOne(1L):");
        System.out.println("--------------------------------");
        System.out.println(asset.toString());
        System.out.println("");


        //Asset asset = new Asset("houseOfCard", "http://localhost:9000/");

        auditLog.audit("Rashid", "Selected: " + "AssetId["+asset.getId()+ "]");

        return new ResponseEntity<Asset>(asset, HttpStatus.OK);
    }


    @RequestMapping(method=RequestMethod.GET, value = "/getAssets")
    public ResponseEntity<List<Asset>> get() {
        // fetch all Assets
        System.out.println("Assets found with findAll():");
        List<Asset> assetlist = new ArrayList();
        System.out.println("-------------------------------");
        for (Asset asset : repository.findAll()) {
            System.out.println(asset.toString());

            assetlist.add(asset);

        }

        Asset asset = new Asset("houseOfCard", "http://localhost:9000/");

        auditLog.audit("Manooj", "Selected");

        return new ResponseEntity<List<Asset>>(assetlist, HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.POST, value = "/createAsset")
    public ResponseEntity<Asset> create(@RequestBody Asset asset) {

        Asset assetNew = new Asset(asset.getAssetName(), asset.getAssetLocation());

        System.out.println("-------------------------------");

        repository.save(assetNew);

        auditLog.audit("Manooj", "Created: " + "AssetId["+assetNew.getId()+ "]");

        return new ResponseEntity<Asset>(assetNew, HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.PUT, value = "/updateAsset")
    public ResponseEntity<Asset> update(@RequestBody Asset assetToupdate) {

        Asset asset = repository.findOne(assetToupdate.getId());
        asset.setAssetName(assetToupdate.getAssetName());
        asset.setAssetLocation(assetToupdate.getAssetName());
        repository.save(asset);
        auditLog.audit("Rashid", "Updated:" + "AssetId["+asset.getId()+ "]");
        return new ResponseEntity<Asset>(asset, HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.DELETE, value = "/deleteAsset")
    public ResponseEntity<Message> delete(@RequestBody Asset asset) {

        repository.delete(asset);
        auditLog.audit("Manooj", "Deleted: " + "AssetId["+asset.getId()+ "]");
        return new ResponseEntity<Message>(new Message(asset.getId(), "Deleted"), HttpStatus.OK);
    }
}
