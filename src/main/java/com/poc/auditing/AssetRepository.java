package com.poc.auditing;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AssetRepository extends CrudRepository<Asset, Long> {

    List<Asset> findByAssetName(String assetName);
}
