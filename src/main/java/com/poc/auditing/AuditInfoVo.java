package com.poc.auditing;

import java.util.Date;

/**
 * Created by rjilani on 10/7/2015.
 */
public class AuditInfoVo {


    private String userName;
    private Date dateTime;
    private String oldValue;
    private String newValue;
    private String action;

    public AuditInfoVo(String userName, Date dateTime, String oldValue, String newValue, String action) {
        this.userName = userName;
        this.dateTime = dateTime;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.action = action;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}
