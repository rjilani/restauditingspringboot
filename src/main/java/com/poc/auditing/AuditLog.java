package com.poc.auditing;

/**
 * Created by rjilani on 10/8/2015.
 */
public interface AuditLog {

   void audit(String user, String message);
}
