package com.poc.auditing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by rjilani on 10/8/2015.
 */

@Component
public class AuditLogImpl implements  AuditLog {

    private static final Logger log = LoggerFactory.getLogger(AuditLogImpl.class);

    @Override
    public void audit(String user, String action) {

        log.info("Audit logging for Asset:");
        AuditInfoVo auditInfoVo = new AuditInfoVo(user, new java.util.Date(),"","", action);
        log.info("-------------------------------");
        log.info(auditInfoVo.getUserName() + " " + auditInfoVo.getDateTime() + " " + auditInfoVo.getAction());
        log.info("-------------------------------");

    }
}
